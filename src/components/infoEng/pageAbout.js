export const engPageRows=[
    {
        row:"About Us",
        cName: "title",

    },
    {
        row:"Over the period of its work, the Sarmat team has gained extensive experience in the following areas:",
        cName: " ",
    },
 
];
export const engPageList=[
    {
        row:"Air transportation",
        cName: "list-link",
        path: "/air-freight",
    },
    {
        row:"Sea transportation",
        cName: "list-link",
        path: "/sea-container-transportation",
    },
    {
        row:"Road transportation",
        cName: "list-link",
        path: "/road-transportation",
    },
    {
        row:"Multimodal transportation",
        cName: "list-link",
        path: "/multimodal-transportation",
    },
    {
        row:"Customs Clearance",
        cName: "list-link",
        path: "/customs-clearance",
    },
]
export const engPageBottom=[
    {
        row:"both for domestic enterprises and international subjects of foreign economic activity.",
        cName: "",

    },
    {
        row:"We have set up a whole transport infrastructure, and after contacting our company, customers no longer need to worry about the optimal cargo delivery ,  its safety or customs clearance.",
        cName: " ",
    },
    {
        row:"The Sarmat company shouldering all the worries itself.",
        cName: " ",
    },
    {
        row:"The key to the successful development of Sarmat has been and remains:",
        cName: "",
    },
    {
        row:"high business activity",
        cName: "order",
    },
    {
        row:"customer focus",
        cName: "order",
    },
    {
        row:"desire for use of innovative technologies",
        cName: "order",
    },
    {
        row:"a team of professionals united by the desire to achieve a common goal - to become the number one partner for their customers in the field of logistics.",
        cName: " ",
    },
 
];