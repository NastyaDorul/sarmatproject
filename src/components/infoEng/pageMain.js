export const engPageRowsMain=[
    {
        row:"Welcome to «Sarmat» Freight forwarding company website!",
        cName: "title",

    },
    {
        row:"«Sarmat» founded in 2019. The company provides freight forwarding services for the international transportation of cargoes by air, road and sea.",
        cName: " ",
    },
    {
        row:"In addition, the company carries out customs clearance of export, import and transit cargoes.",
        cName: " ",
    },
    {
        row:"We cooperate with large companies - shipping carriers that provide our company with high quality and speed of services.",
        cName: " ",
    },
    {
        row:"We offer freight forwarding services that will appreciate our customers who don’t have a logistics service or transport department in their structure.",
        cName: " ",
    },
    {
        row:"Our Benefits:",
        cName: " ",
    },
 
];
export const engPageListMain=[
    {
        row:"Freight forwarding services",
        cName: "list-link",
    },
    {
        row:"Affordable prices",
        cName: "list-link",
    },
    {
        row:"High quality service",
        cName: "list-link",
    },
    {
        row:"Tracking cargo at all stages",
        cName: "list-link",
    },
    {
        row:"Reliability – we provide a high guarantee of cargo safety",
        cName: "list-link",
    },
    {
        row:"Mobility - the ability to connect shipper and consignee of the cargo as soon as possible and at any point on the route",
        cName: "list-link",
    },
]
export const engPageBottomMain=[
    {
        row:"The «Sarmat» Freight forwarding company cooperates with a wide network of warehouse resources and shipping companies, and is also able to choose for its customers the methods that are most advantageous in terms of price and shipping time.",
        cName: "",

    },
    {
        row:"A flexible system of calculating the trucking cost allows us provides an individual approach for each customer. We guarantee a consistently high quality of cargo shipping, and also provide information of the cargo status and its location at any time.",
        cName: " ",
    },
    {
        row:"«Sarmat» Freight forwarding company staff will offer each client an individual cargo delivery scheme that is most convenient and profitable, and make sure that the cargo is shipped to the customer on time and without loss.",
        cName: " ",
    },
    {
        row:"We will be glad to see your company as our customers!",
        cName: "",
    },
    {
        row:"   With best regards and hope for fruitful cooperation, the «Sarmat» team",
        cName: "bold",
    },
];